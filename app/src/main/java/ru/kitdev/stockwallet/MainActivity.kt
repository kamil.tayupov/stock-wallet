package ru.kitdev.stockwallet

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.kitdev.stockwallet.adapter.StockAdapter
import ru.kitdev.stockwallet.data.Stock
import ru.kitdev.stockwallet.domain.FetchStockInteractor
import ru.kitdev.stockwallet.domain.RemoteStockDataSourceProvider
import ru.kitdev.stockwallet.domain.StockMapper
import java.util.Collections

class MainActivity : AppCompatActivity() {

  private val adapter = StockAdapter()

  private val remoteStockDataSourceProvider = RemoteStockDataSourceProvider()
  private val fetchStockInteractor = FetchStockInteractor(remoteStockDataSourceProvider.get(), StockMapper())

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    setupRecyclerView()
  }

  private fun setupRecyclerView() {
    val recyclerView = findViewById<RecyclerView>(R.id.stockRecyclerView)
    recyclerView.adapter = adapter
    fetchStockData()
  }

  @SuppressLint("NotifyDataSetChanged")
  private fun fetchStockData() {
    lifecycleScope.launch(Dispatchers.Main) {
      val symbols = listOf("AAPL", "AMZN", "BABA", "BAC", "DIS", "GOOG", "FB", "IBM", "MSFT", "NFLX", "NVDA", "PYPL", "QCOM", "TSLA", "TWTR", "YHOO", "ZM", "ZNGA")
      val stocks = Collections.synchronizedList<Stock>(mutableListOf())
      for (symbol in symbols) {
        try {
          val stock = withContext(Dispatchers.IO) {
            fetchStockInteractor.fetchStock(symbol)
          }
          stocks.add(stock)
          stocks.sortBy { it.symbol }
          adapter.submitList(stocks) {
            adapter.notifyDataSetChanged()
          }
        } catch (e: Exception) {
          e.printStackTrace()
        }
      }
    }
  }
}