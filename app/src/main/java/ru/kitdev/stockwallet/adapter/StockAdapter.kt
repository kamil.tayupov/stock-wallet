package ru.kitdev.stockwallet.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.kitdev.stockwallet.data.Stock
import ru.kitdev.stockwallet.databinding.ItemStockBinding

class StockAdapter : ListAdapter<Stock, StockAdapter.StockViewHolder>(StockDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
    val binding = ItemStockBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    return StockViewHolder(binding)
  }

  override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
    val stock = currentList[position]
    holder.bind(stock)
  }

  inner class StockViewHolder(private val binding: ItemStockBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(stock: Stock) {
      binding.symbolTextView.text = stock.symbol
      binding.priceTextView.text = stock.price
    }
  }
}
