package ru.kitdev.stockwallet.adapter

import androidx.recyclerview.widget.DiffUtil
import ru.kitdev.stockwallet.data.Stock

class StockDiffCallback : DiffUtil.ItemCallback<Stock>() {

  override fun areItemsTheSame(oldItem: Stock, newItem: Stock): Boolean {
    return oldItem.symbol == newItem.symbol
  }

  override fun areContentsTheSame(oldItem: Stock, newItem: Stock): Boolean {
    return oldItem == newItem
  }
}