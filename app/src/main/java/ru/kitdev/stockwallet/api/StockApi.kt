package ru.kitdev.stockwallet.api

import retrofit2.http.GET
import retrofit2.http.Query

interface StockApi {
  @GET("query?function=TIME_SERIES_DAILY")
  suspend fun getStockQuotes(
    @Query("symbol") symbol: String,
    @Query("apikey") apiKey: String
  ): StockApiModel
}