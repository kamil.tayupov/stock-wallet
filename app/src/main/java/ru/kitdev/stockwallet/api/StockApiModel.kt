package ru.kitdev.stockwallet.api

import com.google.gson.annotations.SerializedName

data class StockApiModel(
  @SerializedName("Meta Data") val metaData: MetaData,
  @SerializedName("Time Series (Daily)") val timeSeries: Map<String, TimeSeriesData>
)