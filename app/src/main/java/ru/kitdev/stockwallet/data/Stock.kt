package ru.kitdev.stockwallet.data

data class Stock(val symbol: String, val price: String)
