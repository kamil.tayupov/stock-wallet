package ru.kitdev.stockwallet.domain

import ru.kitdev.stockwallet.data.Stock

class FetchStockInteractor(
  private val dataSource: RemoteStockDataSource,
  private val mapper: StockMapper
) {
  suspend fun fetchStock(symbol: String): Stock {
    val apiModel = dataSource.fetchStock(symbol)
    return mapper.map(apiModel)
  }
}