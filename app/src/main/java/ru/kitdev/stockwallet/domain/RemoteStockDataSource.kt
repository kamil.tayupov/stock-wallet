package ru.kitdev.stockwallet.domain

import ru.kitdev.stockwallet.api.StockApiModel

interface RemoteStockDataSource {
  suspend fun fetchStock(symbol: String): StockApiModel
}