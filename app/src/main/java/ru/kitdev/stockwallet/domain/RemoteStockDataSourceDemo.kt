package ru.kitdev.stockwallet.domain

import ru.kitdev.stockwallet.api.StockApiModel

private const val API_KEY = "demo"

class RemoteStockDataSourceDemo(
  apiProvider: StockApiProvider
) : RemoteStockDataSource {

  private val api = apiProvider.get()

  override suspend fun fetchStock(symbol: String): StockApiModel {
    return api.getStockQuotes(symbol, API_KEY)
  }
}