package ru.kitdev.stockwallet.domain

class RemoteStockDataSourceProvider {

  private val apiProvider = StockApiProvider()

  private val impl = RemoteStockDataSourceImpl(apiProvider)
  private val demo = RemoteStockDataSourceDemo(apiProvider)

  fun get(): RemoteStockDataSource {
    return demo
  }
}