package ru.kitdev.stockwallet.domain

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.kitdev.stockwallet.api.StockApi

private const val BASE_URL = "https://www.alphavantage.co/"

class StockApiProvider {

  private val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  private val stockApi = retrofit.create(StockApi::class.java)

  fun get(): StockApi {
    return stockApi
  }
}