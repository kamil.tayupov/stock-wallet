package ru.kitdev.stockwallet.domain

import ru.kitdev.stockwallet.api.StockApiModel
import ru.kitdev.stockwallet.data.Stock

class StockMapper {

  fun map(apiModel: StockApiModel): Stock {
    return Stock(apiModel.metaData.symbol, apiModel.timeSeries.entries.first().value.close)
  }
}